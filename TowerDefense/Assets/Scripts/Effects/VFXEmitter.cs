﻿using UnityEngine;

public class VFXEmitter : MonoBehaviour
{
    [SerializeField] private ParticleSystem particle;
    [SerializeField] private Material material;

    public void InstantiateParticle()
    {
        ParticleSystem instantiatedParticle = Instantiate(particle, transform.position, Quaternion.identity);
        instantiatedParticle.GetComponent<Renderer>().material = material;
        instantiatedParticle.Play();
        Destroy(instantiatedParticle, 1f);
    }
}
