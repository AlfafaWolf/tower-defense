﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class MuzzleEffect : MonoBehaviour
{
    [SerializeField] private Transform back;
    [SerializeField] private float duration = 0.1f;

    private List<Transform> children;
    
    private void Awake()
    {
        children = GetComponentsInChildren<Transform>().ToList();
        children.Remove(this.transform);

        SetActiveChildren(false);
    }

    [ContextMenu("Play")]
    public void Play()
    {
        StartCoroutine(PlayEffect());
    }
    
    private IEnumerator PlayEffect()
    {
        SetActiveChildren(true);
        back.RotateAround(back.position, back.forward, Random.Range(0, 45));
        back.localScale = new Vector3(Random.Range(0.8f, 1.1f), Random.Range(0.8f, 1.1f), Random.Range(0.8f, 1.1f));
        
        yield return new WaitForSeconds(duration);

        SetActiveChildren(false);
    }

    private void SetActiveChildren(bool value)
    {
        foreach (var child in children)
        {
            child.gameObject.SetActive(value);
        }
    }
}
