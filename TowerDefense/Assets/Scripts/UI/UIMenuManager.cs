﻿using System.Collections.Generic;
using UnityEngine;

public class UIMenuManager : MonoBehaviour
{
    [SerializeField] private UIPanel currentPanel;
    private Stack<UIPanel> panelHistory = new Stack<UIPanel>();

    private void Awake()
    {
        SetupPanels();
        NavegateToPanel(currentPanel);
    }

    private void SetupPanels()
    {
        UIPanel[] panels = GetComponentsInChildren<UIPanel>();
        foreach (var panel in panels)
        {
            panel.Hide();
        }
    }

    public void NavegateToPanel(UIPanel panel)
    {
        panelHistory.Push(panel);
        SetCurrentPanel(panel);
    }

    public void GoToPreviousPanel()
    {
        if (panelHistory.Count == 0) return;
        
        panelHistory.Pop();
        UIPanel panel = panelHistory.Peek();
        SetCurrentPanel(panel);
    }
    
    public void SetCurrentPanel(UIPanel panel)
    {
        if (!panel) return;

        currentPanel.Hide();

        currentPanel = panel;
        currentPanel.Show();
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
