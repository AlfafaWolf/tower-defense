﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopItemSlotUI : MonoBehaviour
{
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI costText;
    private TowerInfoSO towerInfo;

    public void Initialize(TowerInfoSO tower)
    {
        icon.sprite = tower.Icon;
        costText.text = $"${tower.Cost}";
        towerInfo = tower;
    }
}
