﻿using TMPro;
using UnityEngine;

public class LifeUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI lifeText;
    [SerializeField] private PlayerInfoSO playerInfo;
    
    private void OnEnable()
    {
        playerInfo.HealthSystem.HealthChanged += UpdateLifeText;
        lifeText.text = playerInfo.HealthSystem.Health.ToString();
    }

    private void OnDisable()
    {
        playerInfo.HealthSystem.HealthChanged -= UpdateLifeText;
    }

    private void UpdateLifeText(object sender, HealthChangedArgs e)
    {
        lifeText.text = $"{e.Health}";
    }
}
