﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ShopUI : MonoBehaviour
{
    [SerializeField] private List<TowerInfoSO> shopTowers;
    [SerializeField] private ShopItemSlotUI shopItemSlotPrefab;
    [SerializeField] private TowerSelectedEvent onTowerSelected;
    
    private List<ShopItemSlotUI> itemSlots = new List<ShopItemSlotUI>();

    private void OnEnable()
    {
        itemSlots.Clear();
        foreach (var tower in shopTowers)
        {
            ShopItemSlotUI slot = Instantiate(shopItemSlotPrefab, transform);
            slot.Initialize(tower);

            Button slotButton = slot.GetComponent<Button>();
            slotButton.onClick.AddListener(() => OnSlotSelected(tower));
            
            itemSlots.Add(slot);
        }
    }

    private void OnDisable()
    {
        foreach (var slot in itemSlots)
        {
            Button slotButton = slot.GetComponent<Button>();
            slotButton.onClick.RemoveAllListeners();
        }
    }

    public void OnSlotSelected(TowerInfoSO tower)
    {
        onTowerSelected?.Invoke(tower);
    }
}

[Serializable]
public class TowerSelectedEvent : UnityEvent<TowerInfoSO> { }
