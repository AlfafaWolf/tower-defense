﻿using TMPro;
using UnityEngine;

public class WaveCounterUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI waveCounterText;
    
    private WaveManager waveManager;
    
    private void Awake()
    {
        waveManager = FindObjectOfType<WaveManager>();
        UpdateWaveCounter(0);
    }

    private void OnEnable()
    {
        waveManager.onWaveChange += UpdateWaveCounter;
    }

    private void OnDisable()
    {
        waveManager.onWaveChange -= UpdateWaveCounter;
    }

    public void UpdateWaveCounter(int wave)
    {
        waveCounterText.text = $"{wave}/{waveManager.TotalWaves}";
    }
}
