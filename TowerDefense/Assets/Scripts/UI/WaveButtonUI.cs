﻿using UnityEngine;
using UnityEngine.UI;

public class WaveButtonUI : MonoBehaviour
{
    [SerializeField] private Button button;

    private WaveManager waveManager;

    private void Awake()
    {
        button = GetComponent<Button>();
        waveManager = FindObjectOfType<WaveManager>();
        
        button.onClick.AddListener(waveManager.NextWave);
    }
}
