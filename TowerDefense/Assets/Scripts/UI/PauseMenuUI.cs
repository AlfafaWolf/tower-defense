﻿using System;
using UnityEngine;

public class PauseMenuUI : MonoBehaviour
{
    [SerializeField] private GameObject pausePanel;

    private float defaultTimeScale;
    private float currentTimeScale;
    
    private void Awake()
    {
        pausePanel.SetActive(false);
        Time.timeScale = 1f;
        defaultTimeScale = Time.timeScale;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            ToggleVisibility(!pausePanel.activeSelf);
        }
    }
    
    public void ToggleVisibility(bool value)
    {
        if (value)
        {
            currentTimeScale = Time.timeScale;
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = currentTimeScale;
        }
        pausePanel.SetActive(value);
    }

    private void OnDisable()
    {
        Time.timeScale = defaultTimeScale;
    }
}
