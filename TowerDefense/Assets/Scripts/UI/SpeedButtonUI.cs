﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpeedButtonUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI speedText;
    
    private Button button;
    private float defaultTimeScale;
    private bool isSpeedEnable;

    private void Awake()
    {
        button = GetComponent<Button>();
        defaultTimeScale = Time.timeScale;
        
        button.onClick.AddListener(ToggleSpeedTime);
    }

    public void ToggleSpeedTime()
    {
        if (isSpeedEnable)
        {
            Time.timeScale = defaultTimeScale;
            speedText.text = "2x";
        }
        else
        {
            Time.timeScale = defaultTimeScale * 2;
            speedText.text = "1x";
        }

        isSpeedEnable = !isSpeedEnable;
    }
}
