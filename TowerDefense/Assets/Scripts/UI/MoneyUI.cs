﻿using System;
using TMPro;
using UnityEngine;

public class MoneyUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI moneyText;
    [SerializeField] private PlayerInfoSO playerInfo;
    
    private void OnEnable()
    {
        playerInfo.MoneySystem.OnMoneyChanged += UpdateMoneyText;
        moneyText.text = FormatMoneyText(playerInfo.MoneySystem.Money);
    }

    private void OnDisable()
    {
        playerInfo.MoneySystem.OnMoneyChanged -= UpdateMoneyText;
    }

    public void UpdateMoneyText(object sender, MoneyChanged e)
    {
        moneyText.text = FormatMoneyText(e.NewAmount);
    }

    private string FormatMoneyText(int amount)
    {
        return $"${amount}";
    }
}