﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Tower", menuName = "Tower")]
public class TowerInfoSO : ScriptableObject
{
    [SerializeField] private string towerName;
    [SerializeField] private Sprite icon;
    [SerializeField] private GameObject prefab;
    [Range(1, 99999)]
    [SerializeField] private int cost;

    public string TowerName => towerName;
    public Sprite Icon => icon;
    public GameObject Prefab => prefab;
    public int Cost => cost;
}
