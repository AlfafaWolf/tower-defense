﻿using UnityEngine;

[CreateAssetMenu(menuName = "Enemy")]
public class EnemyDataSO : ScriptableObject
{
    [Range(1, 100)]
    [SerializeField] private float speed = 10f;
    [SerializeField] private int damage = 1;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private int moneyDrop = 10;

    public float Speed => speed;
    public int Damage => damage;
    public GameObject EnemyPrefab => enemyPrefab;
    public int MoneyDrop => moneyDrop;
}
