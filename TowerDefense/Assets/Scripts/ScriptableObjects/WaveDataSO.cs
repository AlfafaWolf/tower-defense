﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Wave")]
public class WaveDataSO : ScriptableObject
{
    public List<Wave> Waves = new List<Wave>();

    public int TotalWaves => Waves.Count;
}

[Serializable]
public struct EnemyGroup
{
    public EnemyDataSO enemyData;
    public int amount;
    public float rate;
}

[Serializable]
public struct Wave
{
    public List<EnemyGroup> groups;
}
