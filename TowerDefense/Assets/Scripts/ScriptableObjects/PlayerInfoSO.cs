﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "Player")]
public class PlayerInfoSO : ScriptableObject, ISerializationCallbackReceiver
{
    [Range(1, 9999)]
    [SerializeField] private int startMoney = 300;
    [Range(1, 9999)]
    [SerializeField] private int startHealth = 100;
    private HealthSystem healthSystem;
    private MoneySystem moneySystem;
    
    public HealthSystem HealthSystem
    {
        get
        {
            if (healthSystem != null) return healthSystem;
            healthSystem = new HealthSystem(startHealth);
            return healthSystem;
        }
    }
    
    public MoneySystem MoneySystem
    {
        get
        {
            if (moneySystem != null) return moneySystem;
            moneySystem = new MoneySystem(startMoney);
            return moneySystem;
        }
    }

    public void OnAfterDeserialize()
    {
        healthSystem = new HealthSystem(startHealth);
        moneySystem = new MoneySystem(startMoney);
    }

    public void Reset()
    {
        healthSystem = new HealthSystem(startHealth);
        moneySystem = new MoneySystem(startMoney);
    }

    public void OnBeforeSerialize() { }

    private void OnDisable()
    {
        Reset();
    }
}