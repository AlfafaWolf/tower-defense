﻿using UnityEngine;

public class ResetPlayerInfoOnLoad : MonoBehaviour
{
    [SerializeField] private PlayerInfoSO playerInfo;

    private void Awake()
    {
        playerInfo.Reset();
    }
}
