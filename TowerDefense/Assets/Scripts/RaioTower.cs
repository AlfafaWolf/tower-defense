﻿using UnityEngine;

public class RaioTower : MonoBehaviour
{
    [SerializeField] private float speed = 100f;
    [SerializeField] private Transform parabolic;
    
    void Update()
    {
        parabolic.RotateAround(parabolic.position, Vector3.up, speed * Time.deltaTime);
    }
}
