﻿using UnityEngine;

public static class LayerMaskExtensions
{
    public static bool CheckIfLayerContains(this LayerMask layerMask, LayerMask otherMask)
    {
        return layerMask == (layerMask | 1 << otherMask);
    }
}
