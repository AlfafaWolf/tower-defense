﻿using UnityEngine;

public class EnemyGivePlayerMoney : MonoBehaviour
{
    [SerializeField] private PlayerInfoSO playerInfo;

    private Enemy enemy;
    
    private void Awake()
    {
        enemy = GetComponent<Enemy>();
    }

    public void GiveMoney()
    {
        playerInfo.MoneySystem.AddMoney(enemy.EnemyData.MoneyDrop);
    }
}
