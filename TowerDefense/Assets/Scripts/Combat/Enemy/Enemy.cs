﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private EnemyDataSO enemyData;

    private Transform pointTarget;
    private int waypointIndex = 0;

    public EnemyDataSO EnemyData => enemyData;
    public static List<Enemy> Enemies = new List<Enemy>();
    public static bool HasEnemiesAlive => Enemies.Count != 0;
    
    private void Start()
    {
        pointTarget = Waypoints.main.points.First();
        StartCoroutine(Move());
        Debug.Log(Enemies.Count);
    }

    private void OnEnable()
    {
        Enemies.Add(this);
    }

    private void OnDisable()
    {
        Enemies.Remove(this);
    }

    IEnumerator Move()
    {
        while (!EnemyReachedFinalDestination())
        {
            Vector3 direction = pointTarget.position - transform.position;
            transform.Translate(direction.normalized * (enemyData.Speed * Time.deltaTime));
            
            if (Vector3.Distance(transform.position, pointTarget.position) <= 0.2f)
            {
                GetNextWaypoint();
            }

            yield return null;
        }
    }

    public void GetNextWaypoint()
    {
        waypointIndex++;
        if (EnemyReachedFinalDestination())
        {
            return;
        }
        pointTarget = Waypoints.main.points[waypointIndex];
    }

    public bool EnemyReachedFinalDestination()
    {
        return waypointIndex >= Waypoints.main.points.Count;
    }

    public static Enemy GetClosestEnemy(Vector3 position, float maxRange)
    {
        Enemy closest = null;
        foreach (var enemy in Enemies)
        {
            if (Vector3.Distance(position, enemy.transform.position) <= maxRange)
            {
                if (closest == null)
                {
                    closest = enemy;
                }
                else
                {
                    if (Vector3.Distance(position, enemy.transform.position) <
                        Vector3.Distance(position, closest.transform.position))
                    {
                        closest = enemy;
                    }
                }
            }
        }

        return closest;
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
