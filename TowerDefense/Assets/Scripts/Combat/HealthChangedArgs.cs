﻿using System;

public class HealthChangedArgs : EventArgs
{
    public int Health { get; }
    public int MaxHealth { get; }

    public HealthChangedArgs(int health, int maxHealth)
    {
        Health = health;
        MaxHealth = maxHealth;
    }
}
