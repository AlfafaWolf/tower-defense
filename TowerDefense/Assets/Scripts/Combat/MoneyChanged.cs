﻿using System;

public class MoneyChanged : EventArgs
{
    public int CurrentAmount { get; }
    public int AddedAmount { get; }
    public int NewAmount => CurrentAmount + AddedAmount;

    public MoneyChanged(int currentAmount, int addedAmount)
    {
        CurrentAmount = currentAmount;
        AddedAmount = addedAmount;
    }
}
