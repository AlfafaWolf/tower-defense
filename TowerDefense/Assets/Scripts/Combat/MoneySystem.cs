﻿using System;

public class MoneySystem
{
    public event EventHandler<MoneyChanged> OnMoneyChanged;
    public int Money => money;
    
    private int money = 300;

    public MoneySystem(int money)
    {
        this.money = money;
    }

    public void AddMoney(int amount)
    {
        if (amount <= 0) return;
        
        OnMoneyChanged?.Invoke(this, new MoneyChanged(money, amount));
        
        money += amount;
    }

    public bool UseMoney(int amount)
    {
        if (amount <= 0) return false;
        if (money - amount < 0) return false;
        
        OnMoneyChanged?.Invoke(this, new MoneyChanged(money, -amount));

        money -= amount;

        return true;
    }
}
