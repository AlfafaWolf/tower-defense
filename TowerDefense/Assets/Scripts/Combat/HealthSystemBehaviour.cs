﻿using UnityEngine;
using UnityEngine.Events;

public class HealthSystemBehaviour : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField] private bool startAtMaxHealth = true;
    [SerializeField] private HealthSystem healthSystem = new HealthSystem(100);

    [Header("Events")]
    [SerializeField] private UnityEvent onDeath = new UnityEvent();
    [SerializeField] private UnityEvent onTakeDamage = new UnityEvent();
    [SerializeField] private UnityEvent onHeal = new UnityEvent();

    public HealthSystem HealthSystem => healthSystem;

    private void Awake()
    {
        healthSystem.OnDeath      += () => onDeath?.Invoke();
        healthSystem.OnTakeDamage += () => onTakeDamage?.Invoke();
        healthSystem.OnHeal       += () => onHeal?.Invoke();
        
        if (startAtMaxHealth)
            healthSystem.Health = healthSystem.MaxHealth;
    }

    public void DealDamage(int amount) => healthSystem.DealDamage(amount);

    public void RestoreHealth(int amount) => healthSystem.RestoreHealth(amount);

#if UNITY_EDITOR
    private void OnValidate()
    {
        healthSystem.Health = Mathf.Clamp(healthSystem.Health, 1, healthSystem.MaxHealth);
    }

    [ContextMenu("Full Health")]
    private void FullHealth()
    {
        healthSystem.Health = healthSystem.MaxHealth;
    }
#endif
}