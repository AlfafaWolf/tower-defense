﻿using UnityEngine;
using UnityEngine.Events;

public class Projectile : MonoBehaviour
{
    [SerializeField] private float speed = 5f;
    [SerializeField] private ParticleSystem explosion;
    [SerializeField] private LayerMask collisionLayer;
    [SerializeField] private LayerMask enemyLayer;
    [SerializeField] private float radius = 1f;
    [SerializeField] private int damage = 75;
    [SerializeField] private UnityEvent onExplode = new UnityEvent();
    
    private Rigidbody rb;
    
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;
    }
    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.IsInLayerMask(collisionLayer))
        {
            onExplode?.Invoke();
            InstantiateExplosion();
            int maxColliders = 10;
            Collider[] hitColliders = new Collider[maxColliders];
            int  numColliders = Physics.OverlapSphereNonAlloc(transform.position, radius, hitColliders);
            for (int i = 0; i < numColliders; i++)
            {
                if (hitColliders[i].TryGetComponent(out HealthSystemBehaviour healthSystem))
                {
                    healthSystem.DealDamage(damage);
                }
            }
            Destroy(this.gameObject);
        }
    }

    public void InstantiateExplosion()
    { 
        ParticleSystem particle = Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(particle, 2f);
    }
    
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radius);
    }
#endif
}
