﻿using System;
using UnityEngine;

[Serializable]
public class HealthSystem : IHaveHealth, IDamageble, IHealable
{
    [SerializeField] private int health = 100;
    [SerializeField] private int maxHealth = 100;

    public int Health
    {
        get => health;
        set => health = value;
    }
    public int MaxHealth
    {
        get => maxHealth;
        set => maxHealth = value;
    }
    public bool IsDead { get; private set; } = false;
    public event Action OnDeath;
    public event Action OnTakeDamage;
    public event Action OnHeal;
    public event EventHandler<HealthChangedArgs> HealthChanged = delegate { };

    public HealthSystem(int health, int maxHealth)
    {
        Health = health <= 0 ? 1 : health;
        MaxHealth = maxHealth;
    }
    
    public HealthSystem(int maxHealth)
    {
        Health = maxHealth;
        MaxHealth = maxHealth;
    }

    public void DealDamage(int amount)
    {
        if (IsDead) { return; }
        
        Health = Mathf.Max(Health - amount, 0);

        OnTakeDamage?.Invoke();
        HealthChanged?.Invoke(this, new HealthChangedArgs(Health, MaxHealth));
        
        if (Health <= 0)
        {
            IsDead = true;
            OnDeath?.Invoke();
        }
    }
    
    public void RestoreHealth(int amount)
    {
        if (IsDead) { return; }
        
        Health = Mathf.Min(Health + amount, MaxHealth);
        
        OnHeal?.Invoke();
        HealthChanged?.Invoke(this, new HealthChangedArgs(Health, MaxHealth));
    }
}
