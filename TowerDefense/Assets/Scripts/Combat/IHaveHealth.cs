﻿using System;

public interface IHaveHealth
{
    int Health { get; }
    int MaxHealth { get; }
    bool IsDead { get; }
    event Action OnDeath;
    event EventHandler<HealthChangedArgs> HealthChanged;
}
