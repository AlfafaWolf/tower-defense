﻿using System.Collections;
using UnityEngine;

public class MissleTurret : Turret
{
    [SerializeField] private GameObject projectile;
    
    protected override IEnumerator ShootEnemy()
    {
        while (true)
        {
            //Enemy enemy = Enemy.GetClosestEnemy(transform.position, range);
            if (currentEnemy != null)
            {
                onShoot?.Invoke();
                //Debug.DrawLine(head.position, enemy.transform.position, Color.magenta, 0.1f);
                //TargetEnemy(enemy);
                muzzle.LookAt(currentEnemy.transform);
                Instantiate(projectile, muzzle.position, muzzle.rotation);
                
                yield return new WaitForSeconds(fireRate);
                
            }
            yield return null;
        }
    }

    public override void TargetEnemy(Enemy enemy) 
    {
        if (enemy != null)
        {
            muzzle.LookAt(enemy.transform);
            head.LookAt(enemy.transform);
            head.transform.eulerAngles = new Vector3(head.transform.eulerAngles.x, head.transform.eulerAngles.y, 0f);
        }
    }
}
