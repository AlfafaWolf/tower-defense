﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Turret : MonoBehaviour
{
    [SerializeField] protected float range = 10f;
    [SerializeField] protected int damage = 25;
    [SerializeField] protected float fireRate = 0.2f;
    [SerializeField] protected LayerMask enemyLayer;
    [SerializeField] protected Transform muzzle;
    [SerializeField] protected Transform head;
    [SerializeField] protected GameObject visualRange;
    [SerializeField] protected UnityEvent onShoot = new UnityEvent();
    
    protected bool isTurretEnable;
    protected Enemy currentEnemy;

    private void Awake()
    {
        visualRange.transform.localScale = new Vector3(range * 2, .1f, range * 2);
    }

    private void Update()
    {
        if (!isTurretEnable) return;
        
        TargetEnemy();
    }

    public void Init()
    {
        isTurretEnable = true;
        StartCoroutine(ShootEnemy());
    }

    protected virtual IEnumerator ShootEnemy()
    {
        while (true)
        {
            //Enemy enemy = Enemy.GetClosestEnemy(transform.position, range);
            if (currentEnemy != null)
            {
                RaycastHit hit;
                if (Physics.Raycast(head.position, head.forward, out hit, range, enemyLayer))
                {
                    //head.LookAt(enemy.transform);
                    //TargetEnemy(enemy);
                    onShoot?.Invoke();
                    //Debug.DrawLine(head.position, enemy.transform.position, Color.magenta, 0.1f);

                    if (hit.transform.TryGetComponent(out HealthSystemBehaviour healthSystem))
                    {
                        healthSystem.DealDamage(damage);
                    }
                    
                    yield return new WaitForSeconds(fireRate);
                }
            }
            yield return null;
        }
    }

    public void TargetEnemy()
    {
        currentEnemy = Enemy.GetClosestEnemy(transform.position, range);
        TargetEnemy(currentEnemy);
    }
    
    public virtual void TargetEnemy(Enemy enemy)
    {
        if (enemy != null)
        {
            head.LookAt(enemy.transform);
        }
    }

    private void OnMouseDown()
    {
        visualRange.SetActive(true);
    }

    private void OnMouseExit()
    {
        if (!isTurretEnable) return;
        
        visualRange.SetActive(false);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, range);
    }
#endif
}
