﻿using System;

public interface IHealable
{
    void RestoreHealth(int amount);
    event Action OnHeal;
}
