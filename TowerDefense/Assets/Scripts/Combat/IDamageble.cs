﻿using System;

public interface IDamageble
{
    void DealDamage(int amount);
    event Action OnTakeDamage;
}
