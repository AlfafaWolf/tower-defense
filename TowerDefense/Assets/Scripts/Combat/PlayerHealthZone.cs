﻿using UnityEngine;
using UnityEngine.Events;

public class PlayerHealthZone : MonoBehaviour
{
    [SerializeField] private PlayerInfoSO playerInfo;
    [SerializeField] private UnityEvent onDeath = new UnityEvent();
    
    private void Awake()
    {
        playerInfo.HealthSystem.OnDeath += Death;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy))
        {
            playerInfo.HealthSystem.DealDamage(enemy.EnemyData.Damage);
            Destroy(enemy.gameObject);
        }
    }

    private void Death()
    {
        onDeath?.Invoke();
    }
}
