﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BuildingManager : MonoBehaviour
{
    public event Action<TowerInfoSO> OnBuildingSelected;
    
    [SerializeField] private LayerMask buildingLayer;
    [SerializeField] private LayerMask uiLayer;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private TowerInfoSO selectedTower;
    [SerializeField] private PlayerInfoSO playerInfo;
    [SerializeField] private UnityEvent onBuildingPlaced;
    

    private Camera cam;
    private readonly float maxDistance = 999f;
    
    private void Awake()
    {
        cam = Camera.main;
    }

    private void Update()
    {
        BuildingInput();
    }

    public void BuildingInput()
    {
        if (Input.GetMouseButtonDown(0) && !IsPointerOverUIElement() && !IsOverABuilding() && selectedTower)
        {
            RaycastHit hit;
            Ray ray = GetCameraRay();

            if (Physics.Raycast(ray, out hit, maxDistance, groundLayer))
            {
                if (playerInfo.MoneySystem.UseMoney(selectedTower.Cost))
                {
                    PlaceBuilding(hit.point);
                    onBuildingPlaced?.Invoke();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.F) || Input.GetMouseButtonDown(1))
        {
            OnTowerSelected(null);
        }
    }

    public void PlaceBuilding(Vector3 position)
    {
        GameObject turret = Instantiate(selectedTower.Prefab, position, selectedTower.Prefab.transform.rotation);
        turret.GetComponent<Turret>().Init();
        
        OnTowerSelected(null);
    }

    public bool IsOverABuilding()
    {
        RaycastHit hit;
        Ray ray = GetCameraRay();

        return Physics.Raycast(ray, out hit, maxDistance, buildingLayer);
    }

    public Ray GetCameraRay()
    {
        return cam.ScreenPointToRay(Input.mousePosition);
    }

    public Vector3? GetRayHitPointToTheGround()
    {
        RaycastHit hit;
        Ray ray = GetCameraRay();

        if (Physics.Raycast(ray, out hit, maxDistance, groundLayer))
        {
            return hit.point;
        }
        else
        {
            return null;
        }
    }
    
    public bool IsPointerOverUIElement()
    {
        var eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);
        return results.Any(r => r.gameObject.IsInLayerMask(uiLayer));
    }

    public void OnTowerSelected(TowerInfoSO tower)
    {
        selectedTower = tower;
        OnBuildingSelected?.Invoke(tower);
    }
}
