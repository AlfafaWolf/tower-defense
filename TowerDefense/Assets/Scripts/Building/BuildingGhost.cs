﻿using System;
using UnityEngine;

public class BuildingGhost : MonoBehaviour
{
    [SerializeField] private BuildingManager buildingManager;

    private GameObject visual;

    private void Awake()
    {
        buildingManager.OnBuildingSelected += RefreshVisual;
    }

    private void LateUpdate()
    {
        UpdateTargetPosition();
    }

    private void UpdateTargetPosition()
    {
        Vector3? targetPosition = buildingManager.GetRayHitPointToTheGround();

        if (targetPosition != null)
        {
            transform.position = targetPosition.Value;
        }
    }

    public void RefreshVisual(TowerInfoSO tower)    
    {
        if (visual != null)
        {
            Destroy(visual.gameObject);
            visual = null;
        }

        if (tower != null)
        {
            visual = Instantiate(tower.Prefab, transform.position, tower.Prefab.transform.rotation);

            Transform visualTransform = visual.transform;
            visualTransform.SetParent(transform);

            visual.layer = gameObject.layer;
            UpdateLayerRecursive(visualTransform);
        }
    }

    private void UpdateLayerRecursive(Transform parent)
    {
        foreach (Transform child in parent)
        {
            child.gameObject.layer = gameObject.layer;
            UpdateLayerRecursive(child);
        }
    }
}
