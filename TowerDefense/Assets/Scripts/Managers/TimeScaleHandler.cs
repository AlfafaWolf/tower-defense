﻿using UnityEngine;

public class TimeScaleHandler : MonoBehaviour
{
    [SerializeField] private float defaultTimeScale = 1f;
    
    private void Awake()
    {
        Time.timeScale = defaultTimeScale;
    }

    public void SetTimeScale(float value)
    {
        if (value < 0) return;
        Time.timeScale = 0;
    }
}
