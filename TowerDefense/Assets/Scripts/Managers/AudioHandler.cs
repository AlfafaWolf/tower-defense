﻿using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    private GameObject audiomanager;
    private AudioSource audioSource;
    
    private void Awake()
    {
        audiomanager = GameObject.FindWithTag("AudioManager");
        if (audiomanager)
        {
            audioSource = audiomanager.GetComponent<AudioSource>();
        }
        else
        {
            Debug.LogWarning("Audio Manager not found.");
        }
    }

    public void PlayOneShot(AudioClip clip)
    {
        if (audioSource)
        {
            audioSource.PlayOneShot(clip);
        }
    }
}
