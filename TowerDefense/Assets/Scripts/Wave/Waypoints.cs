﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using UnityEngine;

public class Waypoints : MonoBehaviour
{
    public List<Transform> points;

    public static Waypoints main;
    
    private void Awake()
    {
        points = GetComponentsInChildren<Transform>().ToList();
        points.Remove(this.transform);

        main = this;
    }
}
