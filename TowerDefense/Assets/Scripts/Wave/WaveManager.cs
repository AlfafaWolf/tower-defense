﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class WaveManager : MonoBehaviour
{
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private WaveDataSO waveData;
    [SerializeField] private UnityEvent onAllWavesEnded;
    
    private int currentWaveIndex;
    private bool hasEnded;
    private bool isInWave;

    public event Action<int> onWaveChange;
    public int TotalWaves => waveData.TotalWaves;

    private void Awake()
    {
        Debug.Log(currentWaveIndex);
    }

    private void Update()
    {
        if (currentWaveIndex >= TotalWaves)
        {
            if (!Enemy.HasEnemiesAlive)
            {
                onAllWavesEnded?.Invoke();
            }
        }
    }

    public void NextWave()
    {
        if (hasEnded) return;
        if (isInWave) return;
        if (Enemy.HasEnemiesAlive)
        {
            if (currentWaveIndex > 0)
            {
                return;
            }
        }
        StartCoroutine(SpawnWaveCoroutine());
    }

    public IEnumerator SpawnWaveCoroutine()
    {
        isInWave = true;
        onWaveChange?.Invoke(currentWaveIndex+1);
        foreach (var enemyGroup in waveData.Waves[currentWaveIndex].groups)
        {
            yield return SpawnEnemyGroupCoroutine(enemyGroup);
        }
        currentWaveIndex++;
        isInWave = false;
        if (currentWaveIndex >= TotalWaves)
        {
            hasEnded = true;
        }
    }

    public IEnumerator SpawnEnemyGroupCoroutine(EnemyGroup enemyGroup)
    {
        for (int i = 0; i < enemyGroup.amount; i++)
        {
            Instantiate(enemyGroup.enemyData.EnemyPrefab, spawnPoint.position, Quaternion.identity);
            yield return new WaitForSeconds(enemyGroup.rate);
        }
    }
}
